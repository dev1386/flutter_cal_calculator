import 'dart:io';

import 'package:calculateurdecalories/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Calculateur de calories'),
      debugShowCheckedModeBanner: false,
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  double poids;
  double age;
  double taille  = 170.0;
  bool genre = false;
  int radioSelected;
  Map mapActivity = {
    0: 'Faible',
    1: 'Modere',
    2: 'Forte'
  };
  int calorieBase;
  int calorieWithActivity;

  @override
  Widget build(BuildContext context) {
    return new GestureDetector(
      onTap: (() => FocusScope.of(context).requestFocus(new FocusNode())),
      child:
      (Platform.isIOS) ?
      new CupertinoPageScaffold(
        child: body(),
      ) :
      new Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
          centerTitle: true,
          backgroundColor: setColor(),
        ),
        body: body(),
      ),
    );
  }

  Widget body() {
    return SingleChildScrollView(
      padding: EdgeInsets.all(15.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Utils.customText(
            'Remplissez tous les champs pour obtenir votre besoin journalier en calories',
          ),
          Utils.padding(),
          new Card(
            elevation: 10.0,
            child: new Column(
              children: <Widget>[
                Utils.padding(),
                new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Utils.customText(
                      'Femme',
                      color: Colors.pink,
                    ),
                    switchByPlatform(),
                    Utils.customText(
                      'Homme',
                      color: Colors.blue,
                    ),
                  ],
                ),
                Utils.padding(),
                ageButton(),
                Utils.padding(),
                Utils.customText(
                  'Votre taille est de ${taille.toInt()} cm',
                  color: setColor(),
                ),
                Utils.padding(),
                sliderByPlatform(),
                Utils.padding(),
                new TextField(
                  keyboardType: TextInputType.number,
                  onChanged: (String string) {
                    setState(() {
                      poids = double.tryParse(string);
                    });
                  },
                  decoration: new InputDecoration(
                    labelText: 'Entrez votre poids en kilos',
                    labelStyle: new TextStyle(
                        color: setColor()
                    ),
                    border: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: setColor(),
                      ),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: setColor(),
                      ),
                    ),
                  ),
                  cursorColor: setColor(),
                ),
                Utils.padding(),
                Utils.customText(
                  'Quelle est votre activité sportive ?',
                  color: setColor(),
                ),
                Utils.padding(),
                rowRadio(),
                Utils.padding(),
              ],
            ),
          ),
          Utils.padding(),
          calcButton(),
        ],
      ),
    );
  }

  Widget switchByPlatform() {
    if(Platform.isIOS) {
      return CupertinoSwitch(
        value: genre,
        activeColor: Colors.blue,
        onChanged: (bool b) {
          setState(() {
            genre = b;
          });
        },
      );
    } else {
      return new Switch(
        value: genre,
        inactiveTrackColor: Colors.pink,
        activeTrackColor: Colors.blue,
        onChanged: (bool b) {
          setState(() {
            genre = b;
          });
        },
      );
    }
  }

  Widget sliderByPlatform() {
    if(Platform.isIOS) {
      return new CupertinoSlider(
        min: 100.0,
        max: 272.0,
        activeColor: setColor(),
        value: taille,
        onChanged: (double d) {
          setState(() {
            taille = d;
          });
        },
      );
    } else {
      return new Slider(
        min: 100.0,
        max: 272.0,
        activeColor: setColor(),
        inactiveColor: Colors.black,
        value: taille,
        onChanged: (double d) {
          setState(() {
            taille = d;
          });
        },
      );
    }
  }

  Widget calcButton() {
    if(Platform.isIOS) {
      return new CupertinoButton(
        child: Utils.customText(
            'Calculer',
            color: Colors.white
        ),
        onPressed: calculate,
        color: setColor(),
      );
    } else {
      return new RaisedButton(
        color: setColor(),
        onPressed: calculate,
        child: Utils.customText(
            'Calculer',
            color: Colors.white
        ),
      );
    }
  }

  Widget ageButton() {
    if(Platform.isIOS) {
      return new CupertinoButton(
        color: setColor(),
        onPressed: (() => datePicker()),
        child: Utils.customText(
          (age == null) ? 'Renseignez votre date de naissance' : 'Vous avez ${age.toInt()} ans',
          color: Colors.white,
        ),
      );
    } else {
      return new RaisedButton(
        color: setColor(),
        onPressed: (() => datePicker()),
        child: Utils.customText(
          (age == null) ? 'Renseignez votre date de naissance' : 'Vous avez ${age.toInt()} ans',
          color: Colors.white,
        ),
      );
    }
  }

  Color setColor() {
    Color color = Colors.pink;
    if(genre) {
      color = Colors.blue;
    }
    return color;
  }

  Future<Null> datePicker() async {
    DateTime choice = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(1900),
      lastDate: DateTime.now(),
      initialDatePickerMode: DatePickerMode.year,
    );

    if(choice != null) {
      var diff = new DateTime.now().difference(choice);
      var ans = (diff.inDays / 365);
      setState(() {
        age = ans;
      });
    }
  }

  Row rowRadio() {
    List<Widget> l = [];
    mapActivity.forEach((k,v) {
      Column column = new Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          new Radio(
            value: k,
            groupValue: radioSelected,
            onChanged: (Object i) {
              setState(() {
                radioSelected = i;
              });
            },
            activeColor: setColor(),
          ),
          Utils.customText(
            v,
            color: setColor(),
          ),
        ],
      );
      l.add(column);
    });
    return new Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: l,
    );
  }

  void calculate() {
    if(age != null && poids != null && radioSelected != null) {
      if (genre) {
        calorieBase = (66.4730 + (13.7516 * poids) + (5.0033 * taille) - (6.7550 * age)).toInt();
      } else {
        calorieBase = (655.0955 + (9.5634 * poids) + (1.8496 * taille) - (4.6756 * age)).toInt();
      }
      switch(radioSelected) {
        case 0:
          calorieWithActivity = (calorieBase * 1.2).toInt();
          break;
        case 1:
          calorieWithActivity = (calorieBase * 1.5).toInt();
          break;
        case 2:
          calorieWithActivity = (calorieBase * 1.8).toInt();
          break;
        default:
          calorieWithActivity = calorieBase;
          break;
      }
      setState(() {
        dialog();
      });
    } else {
      alert();
    }
  }

  Future<Null> dialog() async {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext buildContext) {
        return SimpleDialog(
          title: Utils.customText(
            'VOTRE BESOIN EN CALORIES',
            color: setColor(),
            fontSize: 18.0,
          ),
          contentPadding: EdgeInsets.all(15.0),
          children: <Widget>[
            Utils.padding(),
            Utils.customText('Votre besoin de base est de $calorieBase calories'),
            Utils.padding(),
            Utils.customText('Votre besoin avec une activité sportive est de $calorieWithActivity calories'),
            Utils.padding(),
            new RaisedButton(
              onPressed: () => Navigator.pop(buildContext),
              color: setColor(),
              child: Utils.customText(
                'OK',
                fontSize: 18.0,
                color: Colors.white
              ),
            ),
          ],
        );
      }
    );
  }

  Future<Null> alert() async {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext buildContext) {
        if(Platform.isIOS) {
          return new CupertinoAlertDialog(
            title: Utils.customText(
                'ERREUR',
                fontSize: 20.0,
                color: Colors.red
            ),
            content: Utils.customText('Tous les chapms ne sont pas remplis'),
            actions: <Widget>[
              new CupertinoButton(
                color: Colors.white,
                onPressed: () => Navigator.pop(context),
                child: Utils.customText(
                  'OK',
                  color: Colors.red,
                  fontSize: 18.0,
                ),
              ),
            ],
          );
        } else {
          return new AlertDialog(
            title: Utils.customText(
                'ERREUR',
                fontSize: 20.0,
                color: Colors.red
            ),
            content: Utils.customText('Tous les chapms ne sont pas remplis'),
            actions: <Widget>[
              new FlatButton(
                onPressed: () => Navigator.pop(context),
                child: Utils.customText(
                  'OK',
                  color: Colors.red,
                  fontSize: 18.0,
                ),
              ),
            ],
          );
        }
      },
    );
  }

}
