
import 'dart:io';

import 'package:flutter/material.dart';

class Utils {

  static Widget customText(String data, {color: Colors.black, fontSize: 15.0, textAlign: TextAlign.center}) {
    if(Platform.isIOS) {
      return new DefaultTextStyle(
        style: new TextStyle(
          color: color,
          fontSize: fontSize,
        ),
        child: new Text(
          data,
          textAlign: TextAlign.center,
        ),
      );
    } else {
      return new Text(
        data,
        textAlign: textAlign,
        style: new TextStyle(
          color: color,
          fontSize: fontSize,
        ),
      );
    }
  }

  static Padding padding({left: 0.0, top: 20.0, right: 0.0, bottom: 0.0}) {
    return new Padding(
      padding: EdgeInsets.only(
        left: left,
        top: top,
        right: right,
        bottom: bottom
      ),
    );
  }



}